<?php

/**
 * @file engagement_report.admin.inc
 */


/**
 * Implements hook_FORM_ID().
 */
function engagement_report_admin_form($form, &$form_state) {
  $form = array();
  if (!isset($_SESSION['engagement_table_filter'])) {
    $_SESSION['engagement_table_filter'] = FALSE;
  }
  $filter = $_SESSION['engagement_table_filter'];
  // Show a count of all events.
  $events = engagement_get_event_names();
  $form['event_filter'] = array(
    '#type' => 'fieldset',
    '#title' => 'Event Filter',
    '#collapsible' => TRUE,
  );
  $form['event_filter']['event_select'] = array(
    '#type' => 'select',
    '#title' => 'Events',
    '#description' => t('Select the event to filter the table below'),
    '#default_value' => $filter,
  );
  $form['event_filter']['event_select']['#options'][] = 'no filter';
  foreach ($events as $e) {
    $num_events = engagement_event_count($e->event_name);
    $form['event_filter']['event_select']['#options'][$e->event_name] = $e->event_name;
  }
  $form['event_filter']['event_filter_button'] = array(
    '#type' => 'submit',
    '#value' => 'Filter',
  );

  if ($_SESSION['engagement_table_filter']) {
    $form['event_summary'] = array(
      '#type' => 'fieldset',
      '#title' => t('Summary for <i>:filter</i> event</i>', array(":filter" => $filter)),
    );
    $form['event_summary']['summary_total'] = array(
      '#type' => 'item',
      '#markup' => t('Total count') . ': ' . engagement_event_count_total($filter),
    );
  }


  // We want to show a table of the past events that have occurred.
  $vars = array(
    'header' => array(
      array('data' => 'name', 'field' => 'e.event_name'),
      array('data' => 'value', 'field' => 'e.event_value'),
      array('data' => 'data'),
      array('data' => 'username'),
      array('data' => 'time', 'sort' => 'desc', 'field' => 'e.event_timestamp'),
    ),
  );
  $vars['rows'] = engagement_report_get_rows($vars['header']);
  $form['events_table'] = array(
    '#type' => 'fieldset',
    '#title' => 'Recent Events',
  );
  $form['events_table']['table'] = array(
    '#collapsible' => TRUE,
    '#markup' => theme('table', $vars),
  );
  return $form;
}

function engagement_report_admin_form_submit($form, &$form_state) {
  $v = $form_state['values'];
  $filter = $v['event_select'];
  if ($filter) {
    $_SESSION['engagement_table_filter'] = $filter;
  }
  else {
    unset($_SESSION['engagement_table_filter']);
  }
}

/**
 * Returns a set of rows to be shown in the admin reporting table.
 */
function engagement_report_get_rows($header) {
  $events = db_select('engagement', 'e')
    ->fields('e')
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->condition('event_name', '%' . $_SESSION['engagement_table_filter'] . '%', 'LIKE')
    ->orderByHeader($header)
    ->limit(25)
    ->execute()
    ->fetchAll();
  $rows = array();
  foreach ($events as $e) {
    $u = user_load($e->uid);
    $name = ($u) ? $u->name : 'anonymous';
    $rows[] = array(
      'data' => array(
        $e->event_name,
        $e->event_value,
        var_export(unserialize($e->event_data), TRUE),
        $name,
        format_date($e->event_timestamp),
      ),
    );
  }
  return $rows;
}
