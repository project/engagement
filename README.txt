ABOUT ENGAGEMENT
----------------

This module allows you to track user events as they occur throughout the site.
By doing this, you are able to customize a user's experience to be unique.

The Engagement module can utilize the Google Analytics module to help track
events. Any event that you track in this module can be sent to Google Analytics.


ENGAGEMENT API
--------------

The Engagement module tracks a user's page views and profile updates by default.
You can create custom events very easily in PHP and JavaScript.

In PHP:
Track a custom event using engagement_event_track($event_name, $event_value = NULL, $event_data = NULL, $uid = FALSE);

In JavaScript:
Track a custom event using Drupal.engagement.track(event_type, event_value, event_data);

In HTML:
You can track events on any HTML element by using the data-engagement attribute.
To streamline the process, you must use the engagement_data_attribute($event, $value, $data, $bypassQueue) function.
<a href='#' <?php echo engagement_data_attribute(array('click'), 'submit_form', 'registration_form'); ?>></a>

This 'a' tag will automatically be tracked on it's click event with the inserted event attributes.


EXAMPLES
--------
Let's say we want to change a user's site experience based on the articles they read.
We could make a tag pool of the articles that they view.

First, let's set custom tracking based on tags. In your hook_page_build():
// Get the tags for this article.
$n = menu_get_object();
$tags = $n->field_tags[$n->language];
$tag_array = array();
foreach($tags as $tag){
	$tag_array[] = $tag['tid'];
}
engagement_event_track('article_view', current_path(), array('tags'=>$tag_array), $user->uid);
// Now we are tracking tags, we can do querys on them.

The Engagement module comes with easy-to-use functions for doing common querys. Doing a custom query is easy too because all data is stored locally.

$events = engagement_get_events('article_view', NULL, NULL, $user->uid);
$tags = 0;
foreach($events as $event){
	// Count tags and put in array.
	foreach($event->data['tags'] as $tag){
		$tags[$tag]++;
	}
}
// We now have a user's total engagement for all tags in the $tags array.
// We can customize content for the user with this information.
if($tags['tech'] > 100){
	// Give user the 'tech' badge or something..
}


In JavaScript, we could track when a user hovers over a certain link:
Drupal.engagement.track('mouse_hover', 'secret_button', hover_time);

If you have the Google Analytics module enabled, these events are automagically sent as custom Google events.



AUTHOR INFO
-----------
Created by: donutdan4114

This module is sponsored by Digital Bungalow.
http://www.digitalbungalow.com
