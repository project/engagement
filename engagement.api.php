<?php

/**
 * @file
 * Hooks provided by the Engagement module.
 */

/**
 * Act on an event track before the data is put in the database.
 *
 * @param string $event_name
 *   Name of the event.
 * @param string $event_value
 *   Value of the event.
 * @param string $event_data
 *   Data for the event.
 * @param int $uid
 *   User ID.
 */
function hook_engagement_event_track(&$event_name, &$event_value, &$event_data, &$uid) {
  // Example.
  // Check if we are tracking a 'node_view' event. If we are, add some event data.
  switch ($event_name) {
    case 'node_view':
      $event_data['foo'] = 'bar';
      break;
  }
}

/**
 * Hook into an event deletion before it is deleted.
 *
 * @param string $event_name
 *   Name of the event.
 * @param string $event_value
 *   Value of the event.
 * @param int $uid
 *   User ID.
 */
function hook_engagement_event_delete(&$event_name, &$event_value, &$uid) {
  // Example.
  // Stop event deletion if event_name is 'node_view'.
  switch ($event_name) {
    case 'node_view':
      $uid = -1; // No user has this uid, so no events will be deleted.
      break;
  }
}