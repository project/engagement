/**
 * POST api for engagement event tracking
 * The engagement_token will be output to the page as a Drupal.settings variable
 * The token ensures that all data tracked is valid
 * action is the action to take.. you can do these actions:
 *  event_track
 *  event_get
 * the ACTION is what will determine what vars to send to the API callback
 * Drupal.engagement.track('user_click','this_button');
 * var count = Drupal.engagement.get('user_click','this_button');
 */
(function ($) {

  Drupal.engagement = {
    api_url:"/engagement/api/",
    // Storage object for cookie/localStorage.
    storage:drupal_engagement_storage,
    token:"",
    uid:""
  }

  /**
   * Send event data to the server
   * @param string event_type (i.e. click, hover, mouseover)
   * @param string event_value
   * @param mixed event_data
   */
  Drupal.engagement.track = function (event_type, event_value, event_data) {
    Drupal.engagement.queue.add(event_type, event_value, event_data);
    // Send the event to Google Analytics.
    if (Drupal.engagement.ga() && (event_type != 'page_view')) {
      _gaq.push(['_trackEvent', event_type, event_value, event_data]);
    }
  }

  /**
   * Returns the engagement token value.
   */
  Drupal.engagement.getToken = function () {
    if (token = Drupal.engagement.storage.get('token')) {
      Drupal.engagement.token = token;
      return;
    }
    $.ajax({
      url:Drupal.engagement.api_url + 'get_token',
      success:function (token) {
        Drupal.engagement.storage.set('token', token);
        Drupal.engagement.token = token;
      }
    });
  }

  /**
   * Returns the engagement uid.
   */
  Drupal.engagement.getUID = function () {
    // Check if the cookie is set.
    if (uid = Drupal.engagement.storage.get('uid')) {
      Drupal.engagement.uid = uid;
      return;
    }
    $.ajax({
      url:Drupal.engagement.api_url + 'get_uid',
      success:function (uid) {
        Drupal.engagement.storage.set('uid', uid);
        Drupal.engagement.uid = uid;
      }
    });
  }

  /**
   * Returns true if Google analytics is enabled
   */
  Drupal.engagement.ga = function () {
    return Drupal.settings.engagement_analytics_ga;
  }

  /**
   * Returns true if Site Catalyst is enabled
   */
  Drupal.engagement.sc = function () {
    return Drupal.settings.engagement_analytics_sc;
  }

  /**
   * Binds events to the element.
   */
  Drupal.engagement.bindEvent = function (element, e, event) {
    // Hover is a dirty event, replace with mouseover.
    if (event == 'hover') {
      event = 'mouseover';
    }
    if (event == 'ready') {
      event = 'onload';
    }
    $(element).bind(event, function () {
      Drupal.engagement.track(event, e.value, e.data);
      if (e.bypass) {
        Drupal.engagement.queue.run();
      }
    });
  }

  /**
   * Looks for elements with data-engagement attributes and applies
   * those events to the DOM.
   */
  Drupal.engagement.attachDataEvents = function () {
    var elements = $('[data-engagement]');
    // Foreach element we need to get information.
    elements.each(function (i, o) {
      // Convert the string to JSON, replace slashes with quotes.
      var e = JSON.parse($(o).data('engagement').replace(/\\/g, '"'));
      for (i = 0; i < e.events.length; i++) {
        Drupal.engagement.bindEvent(this, e, e.events[i]);
      }
    });
  }

  /**
   * Queue Object
   * The queue is a stored array of events that need to be sent to the server.
   * Events are stored here to minimize load on the server.
   * The queue should be 'run' on a regular interval.
   */
  Drupal.engagement.queue = {
    // The queue array. Contains events.
    queue:[],
    // Adds an event to the queue.
    add:function (type, value, data) {
      var timestamp = Drupal.engagement.timestamp();
      this.queue.push([type, value, data, timestamp]);
      Drupal.engagement.storage.set('queue', this.queue);
    },
    // Sends the queue to the back-end.
    run:function () {
      if (this.queue.length > 0) {
        $.ajax({
          url:Drupal.engagement.api_url + Drupal.engagement.token + '/' + Drupal.engagement.uid,
          data:"events=" + JSON.stringify(this.queue),
          type:"POST",
          success:function (response) {
            if (response == 'invalid token') {
              Drupal.engagement.resetInfo();
            } else {
              Drupal.engagement.queue.empty();
            }
          }
        });
      }
    },
    // Empties the queue.
    empty:function () {
      this.queue = [];
      Drupal.engagement.storage.set('queue', []);
    },
    // Fetches the queue from storage.
    init:function () {
      var q;
      if (q = Drupal.engagement.storage.get('queue')) {
        this.queue = q;
      }
    }
  }

  /**
   * Simply returns a Unix timestamp.
   */
  Drupal.engagement.timestamp = function () {
    return Math.round((new Date()).getTime() / 1000);
  }


  /**
   * Checks to see if the user just logged in/out and reset the uid/tokens accordingly.
   */
  Drupal.engagement.checkUserSession = function (tokenInvalid) {
    if ($('body').hasClass('not-logged-in') &&
      Drupal.engagement.storage.get('uid') &&
      Drupal.engagement.storage.get('uid').length !== 16) {
      // Need to refresh token/uid.
      Drupal.engagement.resetInfo();
      return;
    }
    if (tokenInvalid) {
      Drupal.engagement.resetInfo();
      return;
    }
  }

  /**
   * Reset the token and UID and get it from the server.
   */
  Drupal.engagement.resetInfo = function () {
    Drupal.engagement.storage.set('uid', 0, -1);
    Drupal.engagement.storage.set('token', 0, -1);
    Drupal.engagement.getToken();
    Drupal.engagement.getUID();
  }

  /**
   * On page load we need to:
   * Check the user session to see if someone is logging in/out
   * Get a token
   * Get the user ID
   * Initialize the queue
   * Start the queue timer
   * Attach data events for elements with the data-engagement attribute
   * Track a page view.
   */
  $(document).ready(function () {
    // Need to check for user login/logout.
    Drupal.engagement.checkUserSession();
    Drupal.engagement.getToken();
    Drupal.engagement.getUID();
    Drupal.engagement.queue.init();
    // We run the queue every 10 seconds. This minimizes the number of
    // requests we need to make to the server.
    setInterval('Drupal.engagement.queue.run()', 5000);
    // Attach data events.
    Drupal.engagement.attachDataEvents();
    // Track page view.
    Drupal.engagement.track('page_view', document.URL);
  });

}(jQuery));
