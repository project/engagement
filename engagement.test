<?php
/**
 * @file engagement.test
 *
 * This file is used for testing purposes.
 */


class EngagementTestCase extends DrupalWebTestCase {

  public static function getInfo() {
    return array(
      'name'        => 'Engagement Core',
      'description' => 'Ensures the engagement core functionality works as intended.',
      'group'       => 'Engagement',
    );
  }

  public function setUp() {
    parent::setUp(array('engagement'));
  }

  /**
   * Test anonymous page tracking.
   */
  public function testEngagementEventCount() {
    $this->addAnonymousEvents();
    $this->assertEqual(4, engagement_event_count(), "Total events equals 4.");
    $this->assertEqual(3, engagement_event_count('test1'), "Number of events called 'test1' equals 3.");
    $this->assertEqual(3, engagement_event_count(NULL, 'test1'), "Number of events with value 'test1' equals 3.");

    $this->addEvents();
    $this->assertEqual(1, engagement_event_count_total('user_new'), "New user event exists.");
    $this->assertEqual(4, engagement_event_count(), "Total events for logged in user is 4.");
    $this->assertEqual(8, engagement_event_count_total(), "Total events equals 8.");
    $this->assertEqual(2, engagement_event_count_total('test2'), "Total events with name 'test2' equals 2.");

    engagement_event_delete('user_new', NULL, $this->loggedInUser->uid);
    $this->assertEqual(FALSE, engagement_event_count_total('user_new'), "New user event deleted.");
    $events = engagement_get_events('test2');
    $this->assertEqual('test1', $events[0]->event_value, "Last event value is 'test1'.");
  }

  /**
   * Add anonymous events.
   */
  private function addAnonymousEvents() {
    $anonymous_uid = user_password(16);
    setcookie('engagement_user_id', $anonymous_uid, REQUEST_TIME + 3600, '/');
    engagement_event_track('test1', 'test1', 'test1', $anonymous_uid, REQUEST_TIME - 60);
    engagement_event_track('test1', 'test2', 'test1', $anonymous_uid, REQUEST_TIME - 50);
    engagement_event_track('test1', 'test1', 'test1', $anonymous_uid, REQUEST_TIME - 40);
    engagement_event_track('test2', 'test1', 'test1', $anonymous_uid, REQUEST_TIME - 30);
  }

  /**
   * Add user events.
   */
  private function addEvents() {
    $user = $this->drupalCreateUser();
    $this->drupalLogin($user);
    engagement_event_track('test3', 'test2', 'test1', $user->uid, REQUEST_TIME - 60);
    engagement_event_track('test2', 'test2', 'test1', $user->uid, REQUEST_TIME - 50);
    engagement_event_track('test3', 'test4', 'test2', $user->uid, REQUEST_TIME - 40);
  }

}
