<?php

/**
 * @file
 * Create the Admin page for the Engagement module.
 */

/**
 * Callback for the engagement admin list page.
 *
 * @return string
 *   System settings form.
 */
function engagement_admin_form($form, &$form_state) {

  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['general_settings']['token'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('engagement_token_value'),
    '#title' => t('Engagement token'),
    '#description' => t('Value passed to token to validate engagement events.'),
  );

  $form['general_settings']['debug_mode'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('engagement_debug_mode'),
    '#title' => t('Debug mode'),
    '#description' => t('Will display tracked events on the screen'),
  );

  // content_type selection list.
  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['tabs']['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracked node types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $types = node_type_get_names();
  $tracked_node_types = engagement_get_tracked_content_types();
  foreach ($types as $type => $title) {
    if (in_array($type, $tracked_node_types)) {
      $checked[] = $type;
    }
    $options[$type] = $title;
  }
  $form['tabs']['content_types']['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Tracked node types"),
    '#description' => t('User activity will be tracked when viewing these node types'),
    '#default_value' => $checked,
    '#options' => $options,
  );


  $form['tabs']['user_roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracked user roles'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $options = array();
  $checked = array();
  $roles = user_roles();
  $tracked_roles = variable_get('engagement_roles_track', array());
  foreach ($roles as $rid => $title) {
    if (in_array($rid, $tracked_roles)) {
      $checked[] = $rid;
    }
    $options[$rid] = $title;
  }
  $form['tabs']['user_roles']['user_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Tracked user roles"),
    '#description' => t('These user roles will be tracked by the module'),
    '#default_value' => $checked,
    '#options' => $options,
  );

  $form['tabs']['dont_track'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page tracking'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['tabs']['dont_track']['dont_track'] = array(
    '#type' => 'textarea',
    '#title' => t('Do not track these pages'),
    '#description' => t('Separate entries by new lines.<br />
        You can use regex here (e.g. node/123, /^node/) See PHP ') .
    l(t('preg_match'), 'http://php.net/manual/en/function.preg-match.php', array('absolute' => TRUE)),
    '#default_value' => implode(PHP_EOL, engagement_get_pages_dont_track()),
  );

  $form['tabs']['dont_track']['usernames'] = array(
    '#type' => 'textarea',
    '#title' => t('Usernames not to track'),
    '#description' => t('Separate entries by new lines.'),
    '#default_value' => implode(PHP_EOL, variable_get('engagement_users_dont_track')),
  );

  $form['tabs']['analytics'] = array(
    '#type' => 'fieldset',
    '#title' => t('Analytics'),
    '#description' => t('Send event tracking to these other tracking programs'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Check if module enabled.
  $disabled = TRUE;
  $description = t("(module not found)");
  if (module_exists('googleanalytics')) {
    $disabled = FALSE;
    $description = t('Manage <a href="!ga_url">Google Analytics</a>', array('!ga_url' => url('admin/config/system/googleanalytics')));
  }
  $form['tabs']['analytics']['google_analytics'] = array(
    '#type' => 'checkbox',
    '#title' => t('Google analytics'),
    '#default_value' => variable_get('engagement_analytics_ga'),
    '#description' => $description,
    '#disabled' => $disabled,
  );
  $disabled = TRUE;
  $description = "(module not found)";

  $form['#submit'][] = 'engagement_admin_form_submit';

  return system_settings_form($form);
}

/**
 * Implements hook_FORM_ID_validate().
 */
function engagement_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (strpos($values['dont_track'], ',') !== FALSE) {
    form_set_error('dont_track', t('Separate by new lines, not commas.'));
  }
  if (strpos($values['usernames'], ',') !== FALSE) {
    form_set_error('usernames', t('Separate by new lines, not commas.'));
  }
  if (!trim(drupal_strlen($values['token']))) {
    form_set_error('token', t('Token value required'));
  }

  // Make sure usernames are valid.
  $usernames = explode(PHP_EOL, $values['usernames']);
  foreach ($usernames as $name) {
    $u = user_load_by_name($name);
    if (!$u) {
      form_set_error('usernames', t("'@name' is not a valid username", array('@name' => $name)));
    }
  }
}

/**
 * Implements hook_FORM_ID_submit().
 */
function engagement_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $content_types = array();
  foreach ($values['content_types'] as $type => $val) {
    if ($val !== 0) {
      $content_types[] = $type;
    }
  }
  $user_roles = array();
  foreach ($values['user_roles'] as $key => $val) {
    if ($val !== 0) {
      $user_roles[] = $val;
    }
  }
  $dont_track = explode(PHP_EOL, $values['dont_track']);

  variable_set('engagement_analytics_ga', $values['google_analytics']);

  /* variable_set('engagement_analytics_sc', $values['site_catalyst']); */
  engagement_save_tracked_content_types($content_types);
  engagement_save_pages_dont_track($dont_track);
  variable_set('engagement_roles_track', $user_roles);
  variable_set('engagement_debug_mode', $values['debug_mode']);
  variable_set('engagement_token_value', $values['token']);
  variable_set('engagement_users_dont_track', explode(PHP_EOL, $values['usernames']));
}
