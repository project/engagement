/**
 * Bulk of this code provided by Ryan Struhl <https://github.com/keisans>
 * Thanks, Ryan!
 */
var drupal_engagement_storage={set:function(a,b,c){if(c===1){window.sessionStorage.setItem("engagement_"+a,JSON.stringify(b))}else{window.localStorage.setItem("engagement_"+a,JSON.stringify(b))}var d=new Date,e="";e=JSON.stringify(b);d.setDate(d.getDate()+c);var f=encodeURIComponent(e)+(c===null?"":"; expires="+d.toUTCString());document.cookie="engagement_"+a+"="+f},get:function(a){if(window.sessionStorage&&window.sessionStorage["engagement_"+a]){return JSON.parse(window.sessionStorage["engagement_"+a])}else if(window.localStorage&&window.localStorage["engagement_"+a]){return JSON.parse(window.localStorage["engagement_"+a])}else{var b,c,d,e=document.cookie.split(";");for(b=0;b<e.length;b+=1){c=e[b].substr(0,e[b].indexOf("="));d=e[b].substr(e[b].indexOf("=")+1);c=c.replace(/^\s+|\s+$/g,"");if(c==="engagement_"+a){var f=decodeURIComponent(d);return JSON.parse(f)}}}}}
